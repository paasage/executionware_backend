
/*
 * Copyright (c) 2015.  CETIC ASBL etienne.Charlier@cetic.be
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 */



package eu.paasage.execware.backend;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

/**
 * Created by ec on 13/03/15.
 *
 * Singleton object holding the configuration parameters loaded from the properties file
 */
public class PaasageConfiguration {

    public static final String RABBITMQ_HOSTNAME = "rabbitmq.hostname";
    public static final String RABBITMQ_PORT = "rabbitmq.port";
    public static final String RABBITMQ_VHOST = "rabbitmq.vhost";
    public static final String RABBITMQ_PASSWORD = "rabbitmq.password";
    public static final String RABBITMQ_USERNAME = "rabbitmq.username";
    public static final String RABBITMQ_EXCHANGE_TYPE = "rabbitmq.exchangeType";
    public static final String RABBITMQ_DURABLE = "rabbitmq.durable";
    public static final String RABBITMQ_AUTOACK = "rabbitmq.autoAck";
    public static final String RABBITMQ_BASICQOS = "rabbitmq.basicQos";
    public static final String RABBITMQ_RETRIES = "rabbitmq.retires";
    public static final String RABBITMQ_MAGMAPPER = "rabbitmq.msgmapper";
    
    public static final String ZMQ_SUBSCRIBER = "zmq.subscriber";
    public static final String ZMQ_CP_GENERATOR_PUBLISH = "zmq.cpgenerator.publish";

    private static final String ENV_CONFIG = "PAASAGE_CONFIG_DIR";
    private static final String DEFAULT_PAASAGE_CONFIG_DIR = ".paasage";
    private static final String EXECWARE_BACKEND_PROPERTIES_FILE_NAME = "eu.paasage.execware.backend.properties";


    // The one and only instance
    private static PaasageConfiguration instance = new PaasageConfiguration();

    // Private constructor. Loads properties files
    private PaasageConfiguration() {
        loadPaasgeProperties();
    }

    //Get the only object available
    public static PaasageConfiguration getInstance() {
        return instance;
    }

    ////////////////////////////////
    // Configuration variables
    private  Properties paasageProperties;

    public Properties getPaasageProperties() {
        return paasageProperties;
    }

    /**
     * Prepare a property file containing default values
     *
     * @return
     */
    private static Properties defautlPaasageProperties() {
        Properties properties = new Properties();
        properties.put(RABBITMQ_HOSTNAME, "10.19.65.101");
        properties.put(RABBITMQ_PORT, "5672");
        properties.put(RABBITMQ_VHOST, "/paasage");
        properties.put(RABBITMQ_PASSWORD, "paasage2015");
        properties.put(RABBITMQ_USERNAME, "paasage");
        properties.put(RABBITMQ_EXCHANGE_TYPE, "direct");
        properties.put(RABBITMQ_DURABLE, true);
        properties.put(RABBITMQ_AUTOACK, false);
        properties.put(RABBITMQ_BASICQOS, true);
        properties.put(RABBITMQ_RETRIES, "5");
        properties.put(RABBITMQ_MAGMAPPER, "json");
        
        properties.put(ZMQ_SUBSCRIBER, "tcp://localhost:5566");
        properties.put(ZMQ_CP_GENERATOR_PUBLISH, "tcp://*:5556");
        
        // FIXME: need more love on how to share the loggers configuration
        // Maybe merging several properties files in the same property bag
        properties.put("log4j.rootLogger", "debug, stdout");
        properties.put("log4j.appender.stdout", "org.apache.log4j.ConsoleAppender");
        properties.put("log4j.appender.stdout.Target", "System.out");
        properties.put("log4j.appender.stdout.layout", "org.apache.log4j.PatternLayout");
        properties.put("log4j.appender.stdout.layout.ConversionPattern", "%d{ABSOLUTE} %5p %c{1}:%L - %m%n");


        return properties;
    }

    /**
     */
    private void loadPaasgeProperties() {

        // Prepare properties object
        this.paasageProperties = new Properties();

        // Load default properties
        Properties defaultProperties = defautlPaasageProperties();
        // Load properties from file
        Properties properties = loadPropertiesFile(EXECWARE_BACKEND_PROPERTIES_FILE_NAME);

        // Merge properties
        this.paasageProperties.putAll(defaultProperties);
        this.paasageProperties.putAll(properties);
        return;
    }

    /**
     * Load a properties Object with contents of the file
     *
     * @param fileName
     * @return
     */
    private static Properties loadPropertiesFile(String fileName) {
        String propertyPath = retrievePropertiesFilePath(fileName);
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(propertyPath));
        } catch (IOException e) {
            // Simply return an empty properties object if error during loading
            properties = new Properties();
        }
        return properties;
    }

    /**
     * Transform the file name into a full path located inside Passage Configuration directory
     *
     * @param propertiesFileName
     * @return
     */
    private static String retrievePropertiesFilePath(String propertiesFileName) {
        Path configPath = Paths.get(retrieveConfigurationDirectoryFullPath());
        return configPath.resolve(propertiesFileName).toAbsolutePath().toString();
    }


    /**
     * Retrieve the fullpath of the Passage configuration directory.
     * First try to get it from the PAASAGE_CONFIG environment variable
     * If it's not set, default to the $HOME/.paasage directory
     *
     * @return
     */
    private static String retrieveConfigurationDirectoryFullPath() {
        String configFullPath = System.getenv(ENV_CONFIG);
        if (configFullPath == null) {
            String home = System.getProperty("user.home");
            Path homePath = Paths.get(home);
            configFullPath = homePath.resolve(DEFAULT_PAASAGE_CONFIG_DIR).toAbsolutePath().toString();
        }
        return configFullPath;
    }
}

