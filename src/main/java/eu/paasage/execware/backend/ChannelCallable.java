/*
 * Copyright (c) 2015.  CETIC ASBL etienne.Charlier@cetic.be
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 */

package eu.paasage.execware.backend;

import java.io.IOException;

import com.rabbitmq.client.Channel;

public interface ChannelCallable<T>
{
    String getDescription();

    T call(Channel channel) throws IOException;
}
