/*
 * Copyright (c) 2015.  CETIC ASBL etienne.Charlier@cetic.be
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 */
package eu.paasage.execware.backend.processors;

import eu.paasage.execware.client.entities.PaasageModel;
import eu.paasage.execware.backend.messages.MessageBase;
import eu.paasage.execware.backend.messages.PaasageMessage;
import eu.paasage.mddb.cdo.client.CDOClient;
import org.apache.commons.io.FileUtils;
import org.eclipse.emf.ecore.EObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

import com.mysql.jdbc.exceptions.jdbc4.CommunicationsException;
import org.eclipse.net4j.db.DBException;//http://mvnrepository.com/artifact/org.eclipselabs/net4j.p2/4.4.0
/**
 * Created by ec on 17/03/15.
 * A new instance of this object is created for each message received from rabbitmq
 */
public class UploadXmiProcessor extends MessageProcessorBase implements MessageProcessor {

    static Logger LOGGER = LoggerFactory.getLogger(UploadXmiProcessor.class.getSimpleName());

    //-------------------------------------------------------------
    //                    static stuff
    //-------------------------------------------------------------
    private static final String MY_MESSAGE_KIND = "UPLOAD_XMI";
    public static void register() {
        registerProcessor(MY_MESSAGE_KIND, UploadXmiProcessor.class);
    }

    //-------------------------------------------------------------
    //                    instance variables
    //-------------------------------------------------------------
    private PaasageMessage paasageMessage = null; // Message read from the queue
    private PaasageModel paasageModel = null;     // Info retrieved from teh execware frontend database

    /**
     * Process message UPLOAD XMI
     * @param message rabbitMQ message to process
     */
    @Override
    public void process(MessageBase message) {

        this.paasageMessage = (PaasageMessage) message;

        LOGGER.info("process called with message: {}", message.toString());
        this.paasageModel = getPaasageModel(this.paasageMessage.getPaasageModelId());

        String xmi = paasageModel.decodeXmiModel();
        String modelName = paasageModel.getName();

        String tempFileName = writeModelToTempfile(xmi);

        if ( tempFileName == null) {
            reportErrorInPaasageModel(paasageModel,"Couldn't write temp file");
            return;
        }
        storeModelInCDO(modelName, tempFileName);
        try {
            FileUtils.forceDelete(new File(tempFileName));
        }
        catch (IOException e) {
            LOGGER.error("Could not remove temporary file");
        }
    }

    //-------------------------------------------------------------
    //                    private methods
    //-------------------------------------------------------------

    /**
     * This methods retrieves the model from the execware database and stores it in the CDOserver
     * @param fileName
     */
    private void storeModelInCDO( String modelName, String fileName) {
        try{
            LOGGER.info("Storing Model named {} from File {} into CDO", modelName, fileName);
            CDOClient client = new CDOClient();
            EObject model = client.loadModel(fileName);
            client.storeModel(model, modelName, true);
            LOGGER.info("Model named {} from File {} stored into CDO", modelName, fileName);
            client.closeSession();
            paasageModel.setAction(PaasageModel.Action.XMI_UPLOADED);
            updatePaasageModel(paasageModel);
        }
        catch (Exception e) {
            LOGGER.error("Could not store model in CDO ", e);
            reportErrorInPaasageModel(this.paasageModel, "Model could not be stored in CDO");
        }
    }

    /**
     * This method writes the model into a temporary file
     * @param fileContents Content to write into the file
     * @return  generated file name
     */
    private String writeModelToTempfile(String fileContents){

        try {
            //FIXME:might be an issue with non UTF-8 encoded xml files
            File tempFile = java.io.File.createTempFile("TEMP-", ".xmi");
            FileUtils.writeStringToFile(tempFile,fileContents, "UTF-8");
            return tempFile.getAbsolutePath();
        }
        catch (IOException e) {
            return null;
        }
    }
}
