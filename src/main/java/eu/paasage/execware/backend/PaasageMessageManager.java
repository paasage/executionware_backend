/*
 * Copyright (c) 2015.  CETIC ASBL etienne.Charlier@cetic.be
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 */

package eu.paasage.execware.backend;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.GetResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ec on 13/03/15.
 */
public class PaasageMessageManager {
    public static final String PAASAGE_INBOX = "PAASAGE";
    public static final String MESSAGE_CONTENT_TYPE = "application/vnd.ccm.pmsg.v1+json";
    public static final String MESSAGE_ENCODING = "UTF-8";



    RabbitMQManager rabbitMQManager;

    public void onApplicationStart() {
        rabbitMQManager.call(new ChannelCallable<AMQP.Exchange.DeclareOk>() {
            @Override
            public String getDescription() {
                return "Declaring direct exchange: " + PAASAGE_INBOX;
            }

            @Override
            public AMQP.Exchange.DeclareOk call(final Channel channel) throws IOException {
                final String exchange = PAASAGE_INBOX;
                final String type = "direct";
                // survive a server restart
                final boolean durable = true;
                // keep it even if not in user
                final boolean autoDelete = false;
                // no special arguments
                final Map<String, Object> arguments = null;

                return channel.exchangeDeclare(exchange, type, durable, autoDelete, arguments);
            }
        });
    }

    public void onDeclareQueue() {
        final String queue = PAASAGE_INBOX;

        rabbitMQManager.call(new ChannelCallable<AMQP.Queue.BindOk>() {
            @Override
            public String getDescription() {
                return "Declaring user queue: " + queue + ", binding it to exchange: "
                        + PAASAGE_INBOX;
            }

            @Override
            public AMQP.Queue.BindOk call(final Channel channel) throws IOException {
                return declarePaasageMessageQueue(queue, channel);
            }
        });
    }

    protected AMQP.Queue.BindOk declarePaasageMessageQueue(final String queue, final Channel channel) throws IOException {
        // survive a server restart
        final boolean durable = true;
        // can be consumed by another connection
        final boolean exclusive = false;
        // keep the queue
        final boolean autoDelete = false;
        // no special arguments
        final Map<String, Object> arguments = null;
        channel.queueDeclare(queue, durable, exclusive, autoDelete, arguments);

        // bind the addressee's queue to the direct exchange
        final String routingKey = queue;
        return channel.queueBind(queue, PAASAGE_INBOX, routingKey);
    }


    public List<String> fetchPaasageMessages() {
        return rabbitMQManager.call(new ChannelCallable<List<String>>() {
            @Override
            public String getDescription() {
                return "Fetching messages for Paasage: ";
            }

            @Override
            public List<String> call(final Channel channel) throws IOException {
                final List<String> messages = new ArrayList<>();

                final String queue = getUserInboxQueue();
                final boolean autoAck = true;

                GetResponse getResponse;

                while ((getResponse = channel.basicGet(queue, autoAck)) != null) {
                    String contentEncoding = getResponse.getProps().getContentEncoding();
                    if (contentEncoding ==null )
                        contentEncoding = "UTF8";
                    final byte[] responseBodyRaw = getResponse.getBody();
                    final String responseBody = new String(responseBodyRaw, contentEncoding);
                    messages.add(responseBody);
                }

                return messages;
            }
        });
    }

    protected String getUserInboxQueue() {
        return PAASAGE_INBOX;
    }

    public void setRabbitMQManager(final RabbitMQManager rabbitMQManager) {
        this.rabbitMQManager = rabbitMQManager;
    }
}

