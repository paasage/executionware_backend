/*
 * Copyright (c) 2015.  CETIC ASBL etienne.Charlier@cetic.be
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 */

package eu.paasage.execware.client.entities.internal;


import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Created by daniel on 22.01.15.
 */
public class Token extends AbstractEntity {

    private String createdOn;
    private String expiresAt;
    private String token;
    private String userId;

    public Token(String createdOn, String expiresAt, String token, String userId) {
        super(null);
        this.createdOn = createdOn;
        this.expiresAt = expiresAt;
        this.token = token;
        this.userId = userId;
    }

    protected Token() {

    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getExpiresAt() {
        return expiresAt;
    }

    public void setExpiresAt(String expiresAt) {
        this.expiresAt = expiresAt;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @JsonIgnore boolean isExpired() {
        return System.currentTimeMillis() > Long.valueOf(this.expiresAt);
    }
}
