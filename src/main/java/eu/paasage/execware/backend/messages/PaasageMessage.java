/*
 * Copyright (c) 2015.  CETIC ASBL etienne.Charlier@cetic.be
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 */

package eu.paasage.execware.backend.messages;

import java.io.Serializable;

/**
 * Created by ec on 7/03/15.
 */
public class PaasageMessage extends MessageBase implements Serializable {
    public PaasageMessage(Long passageModelId, String action){
        super("PAASAGE");
        this.paasageModelId = passageModelId;
        this.action = action;
    }

    public PaasageMessage() {
        super("PAASAGE");
    }

    private Long paasageModelId;
    private String action;

    public Long getPaasageModelId() {
        return paasageModelId;
    }

    public void setPaasageModelId(Long paasageModelId) {
        this.paasageModelId = paasageModelId;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    @Override
    public String toString() {
        return "PaasageMessage{" +
                "paasageModelId=" + paasageModelId +
                ", action='" + action + '\'' +
                '}';
    }

    public static PaasageMessage fromJSON(String messageJSON){
        //FIXME: implement real JSON parsing
        return new PaasageMessage( 1L,"UPLOAD_XMI");
    }
}