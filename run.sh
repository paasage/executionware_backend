#!/bin/bash
CURDIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
JAR=$CURDIR/target/execware-backend-2015.04-SNAPSHOT-jar-with-dependencies.jar
MAIN_CLASS=eu.paasage.execware.backend.ExecWareBackend
DEBUG_OPTIONS='-nodetach -debug -verbose'
DEBUG_OPTIONS='-debug -verbose'
STDOUT="$CURDIR/stdout.log"
STDERR="$CURDIR/stderr.log"
PIDFILE="$CURDIR/backend.pid"
rm $STDOUT $STDERR

jsvc $DEBUG_OPTIONS      \
     -home $JAVA_HOME    \
     -cp   $JAR          \
     -outfile $STDOUT    \
     -errfile $STDERR    \
     -pidfile $PIDFILE   \
     $MAIN_CLASS
