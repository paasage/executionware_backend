/*
 * Copyright (c) 2015.  CETIC ASBL etienne.Charlier@cetic.be
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 */

package eu.paasage.execware.client.entities.internal;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

import javax.annotation.Nullable;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;
import javax.ws.rs.core.MediaType;
import java.io.IOException;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

/**
 * Created by daniel on 22.01.15.
 */
public class AuthenticationFilter implements ClientRequestFilter {

    @Nullable private Token token;
    private final Credential credential;
    private final String baseUrl;

    public AuthenticationFilter(final Credential credential, final String baseUrl) {
        this.credential = credential;
        this.baseUrl = baseUrl;
    }

    protected Token getToken() {
        if (this.token == null || this.token.isExpired()) {
            this.authenticate();
        }
        return this.token;
    }

    private void authenticate() {
        Token token = ClientBuilder.newBuilder().register(JacksonJsonProvider.class).build()
            .target(this.baseUrl + "/login").request(MediaType.APPLICATION_JSON_TYPE).post(
                javax.ws.rs.client.Entity.entity(this.credential, MediaType.APPLICATION_JSON_TYPE))
            .readEntity(Token.class);
        checkState(!checkNotNull(token).isExpired());
        this.token = token;
    }

    @Override public void filter(ClientRequestContext requestContext) throws IOException {
        requestContext.getHeaders().add("X-Auth-Token", this.getToken().getToken());
        requestContext.getHeaders().add("X-Auth-UserId", this.getToken().getUserId());
        requestContext.getHeaders().add("X-Tenant", this.credential.getTenant());
    }
}
