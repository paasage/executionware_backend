/*
 * Copyright (c) 2015.  CETIC ASBL etienne.Charlier@cetic.be
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 */

package eu.paasage.execware.client.entities.internal;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Nullable;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by daniel on 21.01.15.
 */
public abstract class AbstractEntity implements Entity {

    @JsonIgnore @Nullable private List<Link> link;

    @JsonIgnore public List<Link> getLink() {
        return link;
    }

    @JsonProperty public void setLink(@Nullable List<Link> link) {
        this.link = link;
    }

    public AbstractEntity(@Nullable List<Link> link) {
        this.link = link;
    }

    public AbstractEntity() {

    }

    @Override public String getSelfLink() {
        checkNotNull(this.link);
        for (Link link : this.link) {
            if (link.getRel().equals("self")) {
                return link.getHref();
            }
        }
        throw new IllegalStateException("self link not present in entity");
    }
}
