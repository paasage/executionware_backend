/*
 * Copyright (c) 2015.  CETIC ASBL etienne.Charlier@cetic.be
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 */
package eu.paasage.execware.backend.processors;

import eu.paasage.execware.backend.PaasageConfiguration;
import eu.paasage.execware.backend.messages.MessageBase;
import eu.paasage.execware.backend.messages.PaasageMessage;
import eu.paasage.execware.client.entities.PaasageModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zeromq.ZMQ;

/**
 * Created by ec on 25/03/15.
 */
public class StartReasonningProcessor extends MessageProcessorBase implements MessageProcessor {

    private static final String MY_MESSAGE_KIND = "START_REASONNING";

    static Logger LOGGER = LoggerFactory.getLogger(StartReasonningProcessor.class.getSimpleName());

    PaasageConfiguration config = PaasageConfiguration.getInstance();

    public static void register() {

        registerProcessor(MY_MESSAGE_KIND, StartReasonningProcessor.class);

    }

    //-------------------------------------------------------------
    //                    instance variables
    //-------------------------------------------------------------
    private PaasageMessage paasageMessage = null; // Message read from the queue
    private PaasageModel paasageModel = null;     // Info retrieved from teh execware frontend database

    @Override
    public void process(MessageBase message) {
        this.paasageMessage = (PaasageMessage) message;

        LOGGER.info("process called with message: {}", message.toString());

        // get xmi
        this.paasageModel = getPaasageModel(this.paasageMessage.getPaasageModelId());
        String modelName = paasageModel.getName();
        LOGGER.info("Paasage Model Name = "+ modelName);
        String xmi = paasageModel.decodeXmiModel();

        // publish
        ZmqPublisher thread = new ZmqPublisher();
        thread.setMessage(modelName);
        thread.start();

    }

    public class ZmqPublisher extends Thread {

        String message;

        public void setMessage(String message) {
            this.message = message;
        }

        @Override
        public void run() {
            LOGGER.info("ZmqPublisher Thread");
            LOGGER.info("Publish to CP Generator: {}", message.toString());

            publishCPGenerator(message);
        }

        private void publishCPGenerator(String message) {
            try {

                String server = config.getPaasageProperties().getProperty(PaasageConfiguration.ZMQ_CP_GENERATOR_PUBLISH);
                LOGGER.info("Publishing data on " + server);

                ZMQ.Context context = ZMQ.context(1);

                ZMQ.Socket publisher = context.socket(ZMQ.PUB);
                publisher.bind(server);

                LOGGER.info("Bound");
                Thread.sleep(2000);

                publisher.sendMore("ID");
                publisher.send(message);
                LOGGER.info("Message sent: " + message);
                LOGGER.info("About to close: ");
                publisher.close();
                LOGGER.info("About to term: ");
                context.term();
            } catch (Exception ex) {
                LOGGER.error((String) ("Cannot publish on CP Generator: " + ex));
            }

        }
    }

}
