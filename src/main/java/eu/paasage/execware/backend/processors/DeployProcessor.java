
/*
 * Copyright (c) 2015.  CETIC ASBL etienne.Charlier@cetic.be
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 */
package eu.paasage.execware.backend.processors;

import eu.paasage.execware.backend.messages.MessageBase;
import eu.paasage.execware.backend.messages.PaasageMessage;
import eu.paasage.execware.client.entities.PaasageModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author fs
 */
public class DeployProcessor extends MessageProcessorBase implements MessageProcessor {

    static Logger LOGGER = LoggerFactory.getLogger(DeployProcessor.class.getSimpleName());

    //-------------------------------------------------------------
    //                    static stuff
    //-------------------------------------------------------------
    private static final String MY_MESSAGE_KIND = "DEPLOY";
    public static void register() {
        registerProcessor(MY_MESSAGE_KIND, DeployProcessor.class);
    }

    //-------------------------------------------------------------
    //                    instance variables
    //-------------------------------------------------------------
     private PaasageMessage paasageMessage = null; // Message read from the queue
    private PaasageModel paasageModel = null;     // Info retrieved from teh execware frontend database
    

    /**
     * Process message DEPLOY
     * @param message 
     */
    @Override
    public void process(MessageBase message) {

        this.paasageMessage = (PaasageMessage) message;

        LOGGER.info("process called with message: {}", message.toString());
        this.paasageModel = getPaasageModel(this.paasageMessage.getPaasageModelId());

        // update frontend database
        LOGGER.info("Update frontend DB");
        
        this.paasageModel.setState(PaasageModel.State.DEPLOYING);
        updatePaasageModel(paasageModel);
        
    }

    //-------------------------------------------------------------
    //                    private methods
    //-------------------------------------------------------------

}


