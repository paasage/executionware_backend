/*
 * Copyright (c) 2015.  CETIC ASBL etienne.Charlier@cetic.be
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 */

package eu.paasage.execware.backend.processors;

import eu.paasage.execware.client.ClientBuilder;
import eu.paasage.execware.client.ClientController;
import eu.paasage.execware.client.entities.PaasageModel;
import eu.paasage.execware.backend.PaasageConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ec on 17/03/15.
 */
public abstract class MessageProcessorBase implements MessageProcessor {

    private static String FRONT_END_URL = "http://localhost:9000/api";
    //private static String FRONT_END_USER_NAME = "john.doe@example.com";
    private static String FRONT_END_USER_NAME = "backend@paasage.net";
    private static String FRONT_END_PASSWORD = "admin";
    private static String FRONT_END_TENANT = "admin";

    static Logger LOGGER = LoggerFactory.getLogger(MessageProcessorBase.class.getSimpleName());

    private static Map<String,Class<? extends MessageProcessorBase>> processors = new HashMap<>();

    protected static void registerProcessor(String messageKind, Class<? extends MessageProcessorBase> processorClass){
        processors.put(messageKind, processorClass);

        String s = PaasageConfiguration.getInstance().getPaasageProperties().getProperty("frontend.url");
        if ( s != null)
            FRONT_END_URL = s;
        s = PaasageConfiguration.getInstance().getPaasageProperties().getProperty("frontend.username");
        if ( s != null)
            FRONT_END_USER_NAME= s;
        s = PaasageConfiguration.getInstance().getPaasageProperties().getProperty("frontend.password");
        if ( s != null)
            FRONT_END_PASSWORD = s;

    }

    public static MessageProcessor getInstance(String messageKind) {
        Class<? extends MessageProcessorBase> processorClass = processors.get(messageKind);
        if (processorClass == null ) {
            LOGGER.error("No processor registered for messageKind {} :", messageKind);
            return null;
        }

        try {
            return processorClass.newInstance();
        }
        catch (InstantiationException e)
        {
            LOGGER.error("error: Could not create Message processor for messageKind: {}", messageKind);
            return null;
        }
        catch (IllegalAccessException e)
        {
            LOGGER.error("error: Could not create Message processor for messageKind: {}", messageKind);
            return null;
        }
    }

    protected PaasageModel getPaasageModel(Long id) {

        final ClientController<PaasageModel> controller =
                ClientBuilder.getNew()
                        // the base url
                        .url(FRONT_END_URL)
                                // the login credentials
                        .credentials(FRONT_END_USER_NAME, FRONT_END_PASSWORD, FRONT_END_TENANT)
                                // the entity to get the controller for.
                        .build(PaasageModel.class);
        return controller.get(id);
    }

    protected void updatePaasageModel(PaasageModel paasageModel){
        final ClientController<PaasageModel> controller =
                ClientBuilder.getNew()
                        // the base url
                        .url(FRONT_END_URL)
                                // the login credentials
                        .credentials(FRONT_END_USER_NAME, FRONT_END_PASSWORD, FRONT_END_TENANT)
                                // the entity to get the controller for.
                        .build(PaasageModel.class);
        controller.update(paasageModel);
    }

    protected void reportErrorInPaasageModel(PaasageModel paasageModel, String cause){
        LOGGER.error("Message processor reported error: {}", cause);
        //TODO use constant for IN_ERROR
        paasageModel.setState(PaasageModel.State.IN_ERROR);
        paasageModel.setSubState(cause);
        updatePaasageModel(paasageModel);
    }
}
