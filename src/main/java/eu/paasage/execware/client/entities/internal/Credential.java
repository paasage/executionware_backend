/*
 * Copyright (c) 2015.  CETIC ASBL etienne.Charlier@cetic.be
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 */

package eu.paasage.execware.client.entities.internal;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by daniel on 22.01.15.
 */
public class Credential {

    private String email;
    private String password;
    private String tenant;

    public Credential(String email, String password, String tenant) {
        checkNotNull(email);
        checkArgument(!email.isEmpty());
        checkNotNull(password);
        checkArgument(!password.isEmpty());
        checkNotNull(tenant);
        checkArgument(!tenant.isEmpty());
        this.email = email;
        this.password = password;
        this.tenant = tenant;
    }

    protected Credential() {

    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        checkNotNull(email);
        checkArgument(!email.isEmpty());
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        checkNotNull(password);
        checkArgument(!password.isEmpty());
        this.password = password;
    }
    public String getTenant() {
        return tenant;
    }

    public void setTenant(String tenant) {
        checkNotNull(tenant);
        checkArgument(!tenant.isEmpty());
        this.tenant = tenant;
    }

}
