/*
 * Copyright (c) 2015.  CETIC ASBL etienne.Charlier@cetic.be
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 */
package eu.paasage.execware.backend;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.paasage.execware.backend.messages.PaasageMessage;
import eu.paasage.execware.backend.processors.DeployProcessor;
import eu.paasage.execware.backend.processors.MessageProcessor;
import eu.paasage.execware.backend.processors.MessageProcessorBase;
import eu.paasage.execware.backend.processors.NoPlanProcessor;
import eu.paasage.execware.backend.processors.OnePlanProcessor;
import eu.paasage.execware.backend.processors.StartReasonningProcessor;
import eu.paasage.execware.backend.processors.UploadXmiProcessor;
import eu.paasage.execware.backend.processors.UploadedXMIProcessor;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.daemon.Daemon;
import org.apache.commons.daemon.DaemonContext;
import org.apache.commons.daemon.DaemonInitException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zeromq.ZMQ;

/**
 * Created by ec on 11/03/15.
 *
 *
 * /!\ LAUNCH class
 *
 * java -Djava.library.path=/usr/local/lib -cp
 * "/usr/local/share/java/zmq.jar://Users/fs/Programmation/Paasage/executionware_backend/target/executionware-backend-2015.9.1-SNAPSHOT-jar-with-dependencies.jar"
 * eu.paasage.execware.backend.App
 */
public class App implements Daemon {

    static Logger LOGGER = LoggerFactory.getLogger(App.class.getSimpleName());

    /**
     * Convert REASONNED_NO_PLAN|32768 // REASONNED_ONE_PLAN|32768 // DEPLOY|32768 to PaasageMessage
     * @param message
     * @return 
     */
    private static PaasageMessage getPaasageMessage(String message) throws IOException, NumberFormatException {
        LOGGER.info("Getting Paasage Message: {}", message);
        PaasageMessage paasageMessage = null;
        //List<String> list = Arrays.asList("READY_TO_DEPLOY", "DEPLOYED");
        List<String> list = Arrays.asList("REASONNED_NO_PLAN", "REASONNED_ONE_PLAN", "DEPLOY");
        
        if (message.split("\\|").length==2 && 
                list.contains(message.split("\\|")[0])&&
                message.split("\\|")[1].matches("^-?\\d+$")
                ){
            
            paasageMessage = new PaasageMessage();
            paasageMessage.setAction(message.split("\\|")[0]);
            paasageMessage.setName("PaaSage");
            paasageMessage.setPaasageModelId(Long.parseLong(message.split("\\|")[1]));
        }else{
            ObjectMapper mapper = new ObjectMapper();
            paasageMessage = mapper.readValue(message, PaasageMessage.class);
        }
        return paasageMessage;
    }

    private Thread myThread;
    private static boolean stopped = false;

    public static void main(String[] args) {
        LOGGER.info("ExecWareBackend main method called !!!");
        LOGGER.info("PAASAGE_CONFIG_DIR: {}", System.getenv("PAASAGE_CONFIG_DIR"));

        PaasageConfiguration config = PaasageConfiguration.getInstance();

        //TODO: Add annotations or static block
        UploadXmiProcessor.register();
        UploadedXMIProcessor.register();
        StartReasonningProcessor.register();
        NoPlanProcessor.register();
        OnePlanProcessor.register();
        DeployProcessor.register();

        ZMQ.Context context = ZMQ.context(1);
        // Socket to talk to clients
        ZMQ.Socket socket = context.socket(ZMQ.SUB);
        socket.connect(config.getPaasageProperties().getProperty(PaasageConfiguration.ZMQ_SUBSCRIBER));
        socket.subscribe("".getBytes());
        try {
            while (!Thread.currentThread().isInterrupted()) {
                byte[] reply = socket.recv(0);
                String message = new String(reply, StandardCharsets.UTF_8);
                if (message != null) {
                    LOGGER.info("User message received: {}", message);
                    try {
                        PaasageMessage paasageMessage = getPaasageMessage(message);
                        
                        MessageProcessor processor = MessageProcessorBase.getInstance(paasageMessage.getAction());
                        if (processor != null) {
                            LOGGER.info("Processing message");
                            processor.process(paasageMessage);
                            LOGGER.info("Message processed");
                        }

                    } catch (final JsonGenerationException | JsonMappingException e) {
                        LOGGER.error("Json error");
                    } catch (final IOException e) {
                        LOGGER.error("Unexpected error");
                    } catch (Exception e) {
                        StringWriter sw = new StringWriter();
                        PrintWriter pw = new PrintWriter(sw);
                        e.printStackTrace(pw);
                        LOGGER.error(sw.toString());
                    }
                }

            }
        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            LOGGER.error(sw.toString());
        }
        socket.close();
        context.term();

    }

    @Override
    public void init(DaemonContext daemonContext) throws DaemonInitException {

        LOGGER.info("init method called !!!");

        myThread = new Thread() {

            @Override
            public synchronized void start() {
                App.stopped = false;
                super.start();
            }

            @Override
            public void run() {
                main(null);
            }
        };
    }

    @Override
    public void start() throws Exception {
        LOGGER.info("start method called !!!");
        myThread.start();
    }

    @Override
    public void stop() throws Exception {
        LOGGER.info("stop method called !!!");
        App.stopped = true;
        try {
            myThread.join(1000);
        } catch (InterruptedException e) {
            System.err.println(e.getMessage());
            throw e;
        }
    }

    @Override
    public void destroy() {
        LOGGER.info("destroy method called !!!");
        myThread = null;
    }
}
