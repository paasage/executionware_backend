/*
 * Copyright (c) 2015.  CETIC ASBL etienne.Charlier@cetic.be
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 */

package eu.paasage.execware.client;

import eu.paasage.execware.client.entities.internal.Entity;
import eu.paasage.execware.client.entities.internal.Path;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

import static com.google.common.base.Preconditions.*;

/**
 * Created by daniel on 21.01.15.
 */
public class ClientController<T extends Entity> {

    private final Class<T> type;
    private final Client client;
    private final String baseUrl;

    ClientController(Client client, String baseUrl, Class<T> clazz) {
        checkNotNull(client);
        checkNotNull(baseUrl);
        checkArgument(!baseUrl.isEmpty());
        checkNotNull(clazz);
        checkState(clazz.isAnnotationPresent(Path.class));
        this.type = clazz;

        this.baseUrl = baseUrl;
        this.client = client;
    }

    protected Invocation.Builder getRequest(String entityLink) {
        return this.client.target(entityLink).request(MediaType.APPLICATION_JSON);
    }

    public T get(long id) {
        return this
            .getRequest(this.baseUrl + "/" + this.type.getAnnotation(Path.class).value() + "/" + id)
            .get(this.type);
    }

    public List<T> getList() {
        ParameterizedType parameterizedGenericType = new ParameterizedType() {
            public Type[] getActualTypeArguments() {
                return new Type[] {type};
            }

            public Type getRawType() {
                return List.class;
            }

            public Type getOwnerType() {
                return List.class;
            }
        };
        return this.getRequest(this.baseUrl + "/" + this.type.getAnnotation(Path.class).value())
            .get(new GenericType<List<T>>(parameterizedGenericType) {
            });
    }

    public T create(T t) {
        return this.getRequest(this.baseUrl + "/" + this.type.getAnnotation(Path.class).value())
            .post(javax.ws.rs.client.Entity.entity(t, MediaType.APPLICATION_JSON_TYPE))
            .readEntity(type);
    }

    public T update(T t) {
        return this.getRequest(t.getSelfLink())
            .put(javax.ws.rs.client.Entity.entity(t, MediaType.APPLICATION_JSON_TYPE))
            .readEntity(type);
    }

    public void delete(T t) {
        this.getRequest(t.getSelfLink()).delete();
    }


}
