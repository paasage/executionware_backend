/*
 * Copyright (c) 2015.  CETIC ASBL etienne.Charlier@cetic.be
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 */
package eu.paasage.execware.backend.processors;

import eu.paasage.execware.backend.messages.MessageBase;
import eu.paasage.execware.backend.messages.PaasageMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author fs
 */
public class UploadedXMIProcessor extends MessageProcessorBase implements MessageProcessor {

    static Logger LOGGER = LoggerFactory.getLogger(UploadedXMIProcessor.class.getSimpleName());

    //-------------------------------------------------------------
    //                    static stuff
    //-------------------------------------------------------------
    private static final String MY_MESSAGE_KIND = "XMI_UPLOADED";
    public static void register() {
        registerProcessor(MY_MESSAGE_KIND, UploadedXMIProcessor.class);
    }

    //-------------------------------------------------------------
    //                    instance variables
    //-------------------------------------------------------------
    private PaasageMessage paasageMessage = null; // Message read from the queue
    

    /**
     * Process message UPLOADED XMI
     * @param message 
     */
    @Override
    public void process(MessageBase message) {

        this.paasageMessage = (PaasageMessage) message;

        LOGGER.info("process called with message: {}", message.toString());
        
        LOGGER.info("no action defined yet");
        
    }

    //-------------------------------------------------------------
    //                    private methods
    //-------------------------------------------------------------

}
