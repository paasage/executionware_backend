/*
 * Copyright (c) 2015.  CETIC ASBL etienne.Charlier@cetic.be
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 */

package eu.paasage.execware.client;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import eu.paasage.execware.client.entities.internal.AuthenticationFilter;
import eu.paasage.execware.client.entities.internal.Credential;
import eu.paasage.execware.client.entities.internal.Entity;
import org.glassfish.jersey.filter.LoggingFilter;

import javax.ws.rs.client.Client;

/**
 * Created by daniel on 21.01.15.
 */
public class ClientBuilder {

    private String url;
    private Credential credentials;

    private ClientBuilder() {
    }

    public static ClientBuilder getNew() {
        return new ClientBuilder();
    }

    public ClientBuilder url(String url) {
        this.url = url;
        return this;
    }

    public ClientBuilder credentials(String email, String password, String tenant) {
        this.credentials = new Credential(email, password, tenant);
        return this;
    }

    public <T extends Entity> ClientController<T> build(Class<T> clazz) {
        final Client client =
            javax.ws.rs.client.ClientBuilder.newBuilder().register(JacksonJsonProvider.class)
                .register(LoggingFilter.class)
                .register(new AuthenticationFilter(this.credentials, this.url)).build();
        return new ClientController<>(client, this.url, clazz);
    }

}
