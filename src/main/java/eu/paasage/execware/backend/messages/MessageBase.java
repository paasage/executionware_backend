/*
 * Copyright (c) 2015.  CETIC ASBL etienne.Charlier@cetic.be
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 */

package eu.paasage.execware.backend.messages;

/**
 * Created by ec on 16/03/15.
 */

import java.io.Serializable;

import static com.google.common.base.Preconditions.checkNotNull;



/**
 * Created by ec on 7/03/15.
 * Base class for all messages pushed on the messaging ( RabbitMQ)
 */
public abstract class MessageBase implements Serializable {

    public enum State {
        NEW, UNCHANGED, CREATED, READY_TO_REASON, REASONING, NO_SOLUTION, READY_TO_CHOOSE, READY_TO_DEPLOY, DEPLOYING, DEPLOYED, RUNNING;

    }
    public enum Action {
        UNCHANGED,                       // If no arrow in state diagram, don't do anything
        CREATE,                          // Resource being created by user
        UPLOAD_XMI,                      // XMI being uploaded by user
        START_REASONNING,                // Reasoning started by user
        REASONNED_NO_PLAN,               // Reason outcome: NO Deployment PLAN (by PaaSage)
        REASONNED_ONE_PLAN,              // Reason outcome:  One Deployment plan (by PaaSage)
        REASONNED_MULTI_PLANS,           // Reason outcome: Multiple Deployment plans (by PaaSage)
        CHOOSE_PLAN,                     // Plan being chosen by user
        DEPLOY,                          // Deployment started by user
        FINISH_DEPLOYMENT,               // Deployment finished ( by PaaSage)
        RUN;                              // Application start requested by PaaSage

    }

    protected MessageBase(String name) {
        checkNotNull(name);
        this.name = name;
    }

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "MessageBase{" +
                "name='" + name + '\'' +
                '}';
    }
}
