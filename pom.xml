<?xml version="1.0" encoding="UTF-8"?>
<!--
~ Copyright (c) 2015.  CETIC ASBL etienne.Charlier@cetic.be
~
~ This Source Code Form is subject to the terms of the Mozilla Public
~ License, v. 2.0. If a copy of the MPL was not distributed with this file,
~ You can obtain one at http://mozilla.org/MPL/2.0/.
~
-->

<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>org.ow2.paasage</groupId>
        <artifactId>paasage-pom</artifactId>
        <version>2015.9.1-SNAPSHOT</version>
    </parent>
    <groupId>org.ow2.paasage</groupId>
    <artifactId>executionware-backend</artifactId>
    <version>2015.9.1-SNAPSHOT</version>
    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-assembly-plugin</artifactId>
                <version>${maven-assembly-plugin.version}</version>
                <configuration>
                    <archive>
                        <manifest>
                            <mainClass>eu.paasage.execware.backend.App</mainClass>
                        </manifest>
                    </archive>
                    <descriptorRefs>
                        <descriptorRef>jar-with-dependencies</descriptorRef>
                    </descriptorRefs>
                </configuration>
                <executions>
                    <execution>
                        <id>make-assembly</id>
                        <!-- this is used for inheritance merges -->
                        <phase>package</phase>
                        <!-- bind to the packaging phase -->
                        <goals>
                            <goal>single</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>exec-maven-plugin</artifactId>
                <version>1.2.1</version>
                <configuration>
                    <executable>java</executable>
                    <arguments>
                        <argument>-Djava.library.path=/usr/local/lib</argument>
                        <argument>-cp</argument>
                        <argument>"/usr/local/share/java/zmq.jar:/Users/fs/Programmation/Paasage/executionware_backend/target/executionware-backend-2015.9.1-SNAPSHOT-jar-with-dependencies.jar"</argument>
                        <argument>eu.paasage.execware.backend.NewApp</argument>
                    </arguments>
                </configuration>
            </plugin>

        </plugins>
    </build>
    <dependencies>
        <!-- Other PaaSage Component. TODO: use ${paasage.version} in Paasage Depedencies-->
        <dependency>
            <groupId>org.ow2.paasage.mddb.cdo</groupId>
            <artifactId>client</artifactId>
            <version>${paasage.version}</version>
        </dependency>


        <!-- Third parties dependencies -->
        <dependency>
            <groupId>ch.qos.logback</groupId>
            <artifactId>logback-classic</artifactId>
            <version>1.1.2</version>
        </dependency>
        <dependency>
            <groupId>ch.qos.logback</groupId>
            <artifactId>logback-core</artifactId>
            <version>1.1.2</version>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <version>1.7.10</version>
        </dependency>
        <dependency>
            <groupId>commons-daemon</groupId>
            <artifactId>commons-daemon</artifactId>
            <version>1.0.15</version>
        </dependency>
        <dependency>
            <groupId>com.rabbitmq</groupId>
            <artifactId>amqp-client</artifactId>
            <version>3.4.4</version>
        </dependency>
        <dependency>
            <groupId>commons-io</groupId>
            <artifactId>commons-io</artifactId>
            <version>2.4</version>
        </dependency>

        <!-- Common dependencies -->
        <dependency>
            <groupId>org.glassfish.jersey.core</groupId>
            <artifactId>jersey-client</artifactId>
            <version>${jersey.version}</version>
        </dependency>
        <dependency>
            <groupId>org.glassfish.jersey.media</groupId>
            <artifactId>jersey-media-json-jackson</artifactId>
            <version>${jersey.version}</version>
        </dependency>
        <dependency>
            <groupId>com.google.guava</groupId>
            <artifactId>guava</artifactId>
            <version>${guava.version}</version>
        </dependency>
        <dependency>
            <groupId>com.google.code.findbugs</groupId>
            <artifactId>jsr305</artifactId>
            <version>${findbug.version}</version>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>${junit.version}</version>
            <scope>test</scope>
        </dependency>
        
        <!-- Zero MQ -->
        <dependency>
            <groupId>org.zeromq</groupId>
            <artifactId>jzmq</artifactId>
            <version>3.1.0</version>
        </dependency>
       
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>5.1.37</version>
        </dependency>
        
        <dependency>
               <groupId>org.eclipse.net4j</groupId>
  <artifactId>db_4.3.0</artifactId>
  <version>v20140114-0640</version>
        </dependency>
       
    </dependencies>

    <repositories>
        <repository>
            <id>ow2.release</id>
            <name>OW2 repository</name>
            <url>http://repository.ow2.org/nexus/content/repositories/releases/</url>
        </repository>
        <repository>
            <id>ow2.snapshot</id>
            <name>OW2 repository</name>
            <url>http://repository.ow2.org/nexus/content/repositories/snapshots/</url>
        </repository>
        <repository>
            <id>paasage.repo</id>
            <name>PaaSage repository</name>
            <url>http://jenkins.paasage.cetic.be/repository/</url>
        </repository>
    </repositories>

    <distributionManagement>
        <repository>
            <id>ow2.release</id>
            <name>Internal Releases</name>
            <url>http://repository.ow2.org/nexus/content/repositories/releases/</url>
        </repository>
        <snapshotRepository>
            <id>ow2.snapshot</id>
            <name>Internal Releases</name>
            <url>http://repository.ow2.org/nexus/content/repositories/snapshots/</url>
        </snapshotRepository>
    </distributionManagement>
</project>
