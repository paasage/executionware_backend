/*
 * Copyright (c) 2015.  CETIC ASBL etienne.Charlier@cetic.be
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 */



package eu.paasage.execware.backend;

import com.rabbitmq.client.ConnectionFactory;
import eu.paasage.execware.backend.messages.PaasageMessage;
import eu.paasage.execware.backend.processors.MessageProcessor;
import eu.paasage.execware.backend.processors.MessageProcessorBase;
import eu.paasage.execware.backend.processors.UploadXmiProcessor;
import org.apache.commons.daemon.DaemonContext;
import org.apache.commons.daemon.DaemonInitException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.daemon.Daemon;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.List;

/**
 * CURRENTLY NOT USED !!!!!!!!!!!!!!!!!!!
 * CURRENTLY NOT USED !!!!!!!!!!!!!!!!!!!
 * CURRENTLY NOT USED !!!!!!!!!!!!!!!!!!!
 * 
 * @author fs
 */
public class OldAppRabbit implements Daemon {

    static Logger LOGGER = LoggerFactory.getLogger(OldAppRabbit.class.getSimpleName());

    private Thread myThread;
    private static boolean stopped = false;

    public static void main(String[] args) {
        LOGGER.info("ExecWareBackend main method called !!!");

        PaasageConfiguration config = PaasageConfiguration.getInstance();

        final ConnectionFactory factory = new ConnectionFactory();
        factory.setUsername(config.getPaasageProperties().getProperty(PaasageConfiguration.RABBITMQ_USERNAME));
        factory.setPassword(config.getPaasageProperties().getProperty(PaasageConfiguration.RABBITMQ_PASSWORD));
        factory.setVirtualHost(config.getPaasageProperties().getProperty(PaasageConfiguration.RABBITMQ_VHOST));
        factory.setHost(config.getPaasageProperties().getProperty(PaasageConfiguration.RABBITMQ_HOSTNAME));
        String sPort = config.getPaasageProperties().getProperty(PaasageConfiguration.RABBITMQ_PORT);
        int port = Integer.parseInt(sPort);
        factory.setPort(port);

        //TODO: Add annotations or static block
        UploadXmiProcessor.register();

        // simulate dependency management creation and wiring
        final RabbitMQManager rabbitMqManager = new RabbitMQManager(factory);
        rabbitMqManager.start();

        final PaasageMessageManager paasageMessageManager = new PaasageMessageManager();
        paasageMessageManager.setRabbitMQManager(rabbitMqManager);

        paasageMessageManager.onApplicationStart();
        paasageMessageManager.onDeclareQueue();


        while (!Thread.currentThread().isInterrupted())
        {
            try
            {
                final List<String> messages = paasageMessageManager.fetchPaasageMessages();
                if (messages != null) {
                    for (final String message : messages) {
                        LOGGER.info("User message received: {}", message);
                        ObjectMapper mapper = new ObjectMapper();
                        try {
                            PaasageMessage paasageMessage = mapper.readValue(message, PaasageMessage.class);
                            MessageProcessor processor = MessageProcessorBase.getInstance(paasageMessage.getAction());
                            if (processor != null) {
                                processor.process(paasageMessage);
                            }
                        } catch ( final JsonGenerationException | JsonMappingException e) {
                            System.out.println(e);
                        } catch (final IOException e) {
                            System.out.println(e);
                        }
                    }
                }
            }
            catch (final Exception e)
            {
                e.printStackTrace();
            }
            try
            {
                Thread.sleep(1000L);
            }
            catch (final InterruptedException ie)
            {
                Thread.currentThread().interrupt();
            }
        }
    }

    @Override
    public void init(DaemonContext daemonContext) throws DaemonInitException {

        LOGGER.info("init method called !!!");

        myThread = new Thread(){

            @Override
            public synchronized void start() {
                OldAppRabbit.stopped = false;
                super.start();
            }

            @Override
            public void run() {
                main(null);
            }
        };
    }

    @Override
    public void start() throws Exception {
        LOGGER.info("start method called !!!");
        myThread.start();
    }

    @Override
    public void stop() throws Exception {
        LOGGER.info("stop method called !!!");
        OldAppRabbit.stopped = true;
        try{
            myThread.join(1000);
        }catch(InterruptedException e){
            System.err.println(e.getMessage());
            throw e;
        }
    }

    @Override
    public void destroy() {
        LOGGER.info("destroy method called !!!");
        myThread=null;
    }
}
