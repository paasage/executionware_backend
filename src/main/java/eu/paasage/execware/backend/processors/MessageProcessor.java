/*
 * Copyright (c) 2015.  CETIC ASBL etienne.Charlier@cetic.be
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 */

package eu.paasage.execware.backend.processors;


import eu.paasage.execware.backend.messages.MessageBase;

/**
 * Created by ec on 16/03/15.
 */
public interface MessageProcessor {
    void process(MessageBase message);
}
